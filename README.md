#PaperCutter - A Papercut Attack Tool

PaperCutter is a dictionary attack tool for Papercut print management software.

**Note:** This is a "proof of concept" and is for informational purposes only. Do not use without permission from an administrator.



#Copyright and License

Copyright (c) 2012 Daniel Ng

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-nc-sa/3.0/).