import xmlrpclib
import urllib2

class PapercutConnection(xmlrpclib.Server):  
  def __init__(self, uri, xmlrpc="/rpc/clients/xmlrpc"):
    self.transport = UrllibTransport()
    xmlrpclib.Server.__init__(self, uri=uri+xmlrpc, transport=self.transport)
    
    self.connected = self.ping()
  
  def set_proxy(self, proxy):
    self.transport.set_proxy(proxy)
  
  def ping(self):
    try:
      if self.client.ping():
        return True
    except urllib2.HTTPError, e:
      print "[x]", e
      print "[!] Not connected to a Papercut server..."
    except Exception, e:
      print "[x]", e.__class__, e
      print "[!] Not connected to a Papercut server..."
    else:
      return False
  
  def getGlobalConfig(self):
    return self.client.getGlobalConfig("","") if self.connected else None
  
  def getUserConfig(self, user):
    return self.client.getUserConfig(user, "", "") if self.connected else None
  
  def userExists(self, user):
    return self.client.userExists(user) if self.connected else None
  
  def getUserBalance(self, user):
    return self.client.getUserBalance(user, "", "", user) if self.connected else None
  
  def getUserDetails(self, name_or_id, is_id=False):
    return self.client.getUserDetails("", "", "", name_or_id, is_id) if self.connected else None
  
  def getUserMessages(self, user):
    return self.client.getUserMessages(user, "", "") if self.connected else None
  
  def getPendingPrintJobs(self, user):
    return self.client.getPendingPrintJobs(user, "", "") if self.connected else None
  
  def getPendingPrintJobCount(self, user):
    return self.client.getPendingPrintJobCount(user, "", "") if self.connected else None
  
  def getClientDialogs(self, user):
    return self.client.getClientDialogs(user, "", "") if self.connected else None
  
  def getChargeRates(self, user):
    return self.client.getChargeRates3(user, "", "", "", 2) if self.connected else None
  
  def checkIdentity(self, username, password):
    return self.client.checkIdentity(username, password) if self.connected else None
  
  def getUsersRecentAccounts(self, user, job_id):
    return self.client.getUsersRecentAccounts(user, "", "", job_id) if self.connected else None
  
class UrllibTransport(xmlrpclib.Transport):
  urlopener = urllib2.build_opener(urllib2.ProxyHandler({}))
  
  def set_proxy(self, proxy):
    print "[~] Using proxy %s" % proxy
    if proxy is not None: 
      proxy_handler = urllib2.ProxyHandler({'http' : proxy, 'https' : proxy})
    else: 
      proxy_handler = urllib2.ProxyHandler({})
      
    self.urlopener = urllib2.build_opener(proxy_handler)
              
  def request(self, host, handler, request_body, verbose=0):
    self.urlopener.addheaders = [('User-agent', 'Papercut NG')]
    f = self.urlopener.open("http://%s%s" % (host, handler), request_body, 10)
    self.verbose = verbose 
    return self.parse_response(f)
  
"""
Methods supported by the papercut rpc server with some info I discovered
Indented methods were used in my papercut rpc client

CLIENT_HANDLER_NAME = "client";
METHOD_PREFIX = "client.";
  METHOD_GET_GLOBAL_CONFIG = "client.getGlobalConfig"; #unknown, unknown
  METHOD_GET_USER_CONFIG = "client.getUserConfig"; #username, computer name, all IP addresses
  METHOD_USER_EXISTS = "client.userExists"; #username
METHOD_PENDING_ACTIONS = "client.getPendingActions";
  METHOD_GET_USER_BALANCE = "client.getUserBalance"; #username, computer name, all IP addresses, username (both usernames are required or will throw error)
  METHOD_GET_USER_DETAILS = "client.getUserDetails"; #username, computer name, all IP addresses, username/id to check, boolean: is id passes
  METHOD_GET_USER_MESSAGES = "client.getUserMessages"; #username, computer name, all IP addresses
  METHOD_GET_PENDING_PRINT_JOBS = "client.getPendingPrintJobs"; #username, computer name, all IP addresses
  METHOD_GET_PENDING_PRINT_JOB_COUNT = "client.getPendingPrintJobCount"; #username, computer name, all IP addresses
METHOD_GET_PRINT_JOB_INFO = "client.getPrintJobInfo";
METHOD_UPDATE_PENDING_PRINT_JOB = "client.updatePendingPrintJob";
METHOD_UPDATE_ALL_PENDING_PRINT_JOBS = "client.updateAllPendingPrintJobs";
METHOD_GET_PRINT_JOB_POPUP_CONFIG = "client.getPrintJobPopupConfig";
  METHOD_GET_CLIENT_DIALOGS = "client.getClientDialogs"; #username, computer name, all IP addresses
METHOD_SET_CLIENT_DIALOG_RESPONSE = "client.setClientDialogResponse";
METHOD_GET_USER_ACCOUNTS_FILE = "client.getUserAccountsFile";
METHOD_GET_ALL_SHARED_ACCOUNTS_FILE = "client.getAllSharedAccountsFile";
METHOD_GET_USER_ACCOUNTS_LAST_MODIFIED_TIME = "client.getUserAccountsLastModifiedTime";
METHOD_AUTHENTICATE_USER = "client.authenticateUser";
METHOD_AUTHENTICATE_USER_WITH_COOKIE = "client.authenticateUserWithCookie";
METHOD_AUTHENTICATE_USER_WITH_SHARED_SECRET = "client.authenticateUserWithSharedSecret";
METHOD_CLEAR_AUTHENTICATION = "client.clearAuthentication";
  METHOD_CHECK_IDENTITY = "client.checkIdentity"; #username, password
  METHOD_GET_USERS_RECENT_ACCOUNTS = "client.getUsersRecentAccounts"; #username, computer name, all IP addresses, job id
METHOD_GET_USERS_PREFERERED_ACCOUNTS = "client.getUsersPreferredAccounts";
METHOD_SET_USERS_PREFERERED_ACCOUNTS = "client.setUsersPreferredAccounts";
  METHOD_GET_CHARGE_RATES = "client.getChargeRates3"; #username, computer name, all IP addresses, unknown int
  METHOD_PING = "client.ping";
FIELD_ACCOUNT_TO_CHARGE_TYPE = "account-to-charge-type";
FIELD_USER_NAME = "username";
FIELD_PASSWORD = "password";
FIELD_ACCOUNT_PIN = "account-pin";
FIELD_ACCOUNT_ID = "account-id";
FIELD_JOB_COMMENT = "job-comment";
FIELD_JOB_INVOICE = "job-invoice";
FIELD_JOB_CHARGE_RATE = "charge-rate";
AUTH_TTL_SECS_FOREVER_WITH_COOKIE = -2;
"""
